buildscript {
    repositories {
        mavenCentral()
    }

    val kotlinVersion by extra("1.6.0")

    dependencies {
        classpath(kotlin("gradle-plugin", version = kotlinVersion))
        classpath(kotlin("serialization", version = kotlinVersion))
    }
}

val kotlinVersion = extra["kotlinVersion"]

plugins {
    kotlin("plugin.serialization") version "1.6.0" apply false
}

val gitLabPrivateToken = project.properties["gitLabPrivateToken"]
val gitLabCiJobToken = project.properties["gitLabCiJobToken"]

allprojects {

    repositories {
        mavenCentral()
        if (project.properties["useMavenLocal"] == "true") {
            mavenLocal()
        }
    }
}

subprojects {
    val kotlinVersion by extra("1.6.0")
    val kotlinWrappersVersion by extra("pre.255-kotlin-1.5.31")
    val kotlinxSerializationVersion by extra("1.3.0")

    afterEvaluate {
        if (plugins.hasPlugin("kotlin-multiplatform")) {
            configure<org.jetbrains.kotlin.gradle.dsl.KotlinMultiplatformExtension> {
                sourceSets {
                    all {
                        languageSettings.languageVersion = "1.5"
                        languageSettings.apiVersion = "1.5"
                        languageSettings.optIn("kotlin.RequiresOptIn")
                    }
                }
            }
        }
    }
}

// https://youtrack.jetbrains.com/issue/KT-49124
rootProject.plugins.withType<org.jetbrains.kotlin.gradle.targets.js.yarn.YarnPlugin> {
    rootProject.the<org.jetbrains.kotlin.gradle.targets.js.yarn.YarnRootExtension>().apply {
        resolution("@webpack-cli/serve", "1.5.2")
    }
}
