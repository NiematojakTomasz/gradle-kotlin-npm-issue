pluginManagement {
    repositories {
        mavenCentral()
        google()
        maven { url = uri("https://plugins.gradle.org/m2/") }
    }
}

rootProject.name = "gradle-kotlin-npm-issue"

include("app")
//include("kotlinx-nodejs")
