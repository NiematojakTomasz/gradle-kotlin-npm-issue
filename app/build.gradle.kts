import org.jetbrains.kotlin.gradle.targets.js.nodejs.NodeJsExec

plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization")
}

val ktorVersion = "1.6.4"

val jsPlatformType = Attribute.of("jsPlatformType", String::class.java)

kotlin {
    js("browser", IR) {
        browser {
        }
        compilations["main"].defaultSourceSet.dependsOn(js(IR).compilations["main"].defaultSourceSet)
        configurations.all { if(name == compilations["main"].apiConfigurationName + "Elements") attributes.attribute(jsPlatformType, "browser") }
        configurations.all { if(name == "browserRuntimeElements") attributes.attribute(jsPlatformType, "browser") }
        binaries.executable()
    }
    js("node", IR) {
        nodejs { }
        compilations["main"].defaultSourceSet.dependsOn(js(IR).compilations["main"].defaultSourceSet)
        configurations.all { if(name == compilations["main"].apiConfigurationName + "Elements") attributes.attribute(jsPlatformType, "node") }
        configurations.all { if(name == "nodeRuntimeElements") attributes.attribute(jsPlatformType, "node") }
        binaries.executable()
    }
    sourceSets {
        commonMain {
            dependencies {
            }
        }
        commonTest {
            dependencies {
            }
        }
        js().compilations["main"].defaultSourceSet {
            dependencies {
            }
        }
        js("node").compilations["main"].defaultSourceSet {
            dependencies {
            }
        }
    }
}

val config: String? by project
if (config != null) {
    tasks.withType(NodeJsExec::class).all {
        nodeArgs.add("--trace-warnings")
        args("--config", config)
    }
}

tasks.named("compileKotlinJs") {
    enabled = false
}

tasks.getByName<ProcessResources>("nodeProcessResources") {
    val webpackTask = tasks.getByName<org.jetbrains.kotlin.gradle.targets.js.webpack.KotlinWebpack>("browserBrowserProductionWebpack")
    dependsOn(webpackTask)
    into("static") {
        from(File(webpackTask.destinationDirectory, webpackTask.outputFileName))
        from(File(webpackTask.destinationDirectory, webpackTask.outputFileName + ".map"))
    }
    println(this.javaClass.canonicalName)
}
